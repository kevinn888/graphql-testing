const root = require("./RootTypeDef");
const authTypeDef = require("./AuthTypeDef");
const userTypeDef = require("./UserTypeDef");
const productTypeDef = require("./ProductsTypeDef");
const vendorTypeDef = require("./VendorTypeDef");

const typeDefs = [
  root,
  userTypeDef,
  authTypeDef,
  productTypeDef,
  vendorTypeDef,
];

module.exports = typeDefs;
