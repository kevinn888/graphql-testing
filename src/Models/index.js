const fs = require("fs");
const path = require("path");
const Sequelize = require("sequelize");
const config = require("../Config/DBConfig");

const basename = path.basename(__filename);
const db = {};
let sequelize;
if (process.env.CLEARDB_DATABASE_URL) {
  sequelize = new Sequelize(process.env.CLEARDB_DATABASE_URL);
} else {
  sequelize = new Sequelize(
    config.Database,
    config.DBUsername,
    config.DBPassword,
    {
      username: config.DBUsername,
      password: config.DBPassword,
      database: config.Database,
      host: config.DBHost,
      port: config.DBPort,
      dialect: config.Dialect,
      logging: false,
    }
  );
}
sequelize
  .authenticate()
  .then(() => {
    console.log("Connection has been established successfully.");
  })
  .catch((err) => {
    console.error("Unable to connect to the database:", err);
  });

fs.readdirSync(__dirname)
  .filter(
    (file) =>
      file.indexOf(".") !== 0 && file !== basename && file.slice(-3) === ".js"
  )
  .forEach((file) => {
    const model = sequelize.import(path.join(__dirname, file));
    db[model.name] = model;
  });

Object.keys(db).forEach((modelName) => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

db.sequelize = sequelize;
module.exports = db;
