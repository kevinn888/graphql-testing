module.exports = (sequelize, DataTypes) => {
  const Product = sequelize.define("Product", {
    Product_ID: {
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      validate: {
        notEmpty: {
          msg: "Ticket cannot be empty",
        },
      },
      allowNull: false,
    },
    ProductName: DataTypes.STRING,
    Quantity: DataTypes.STRING,
    Vendor_ID: DataTypes.STRING,
    createdAt: DataTypes.STRING,
    updatedAt: DataTypes.STRING,
    deletedAt: DataTypes.STRING,
  });
  Product.associate = (models) => {
    Product.belongsTo(models.Vendor, { foreignKey: "Vendor_ID" });
  };
  return Product;
};
