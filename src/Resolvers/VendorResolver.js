const authenticated = require("../Utils/Authorization");

const resolvers = {
  Query: {
    vendors: authenticated((parent, args, { db }) => db.Vendor.findAll()),
    vendor: authenticated((parent, args, { db }) =>
      db.Vendor.findByPk(args.Vendor_ID)
    ),
  },
  Mutation: {
    newVendor: aunthenticated(async (parent, args, { db, redis }) => {
      const timestamp = Math.round(new Date().getTime() / 1000);
      const lastId = await redis.get("vendorId");
      const modelId = parseInt(lastId) + 1;
      const vendor = {
        Vendor_ID: "VDR-" + String(modelId),
        VendorName: args.VendorName || "",
        PhoneNumber: args.PhoneNumber || "",
        address: args.address || "",
        createdAt: timestamp.toString(),
        updatedAt: timestamp.toString(),
        deletedAt: "",
      };
      const check = await db.Vendor.create(vendor);
      if (check) {
        await redis.set("vendorId", modelId);
      } else {
        console.error("error create record");
      }
      return check;
    }),
  },
};

module.exports = resolvers;
