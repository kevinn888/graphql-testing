const { gql } = require("apollo-server");

const authTypeDef = gql`
  extend type Query {
    whoami: User
  }

  extend type Mutation {
    authenticate(email: String!, password: String!): AuthResponse
  }

  type AuthResponse {
    accessToken: String!
  }
`;
module.exports = authTypeDef;
