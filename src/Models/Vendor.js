module.exports = (sequelize, DataTypes) => {
  const Vendor = sequelize.define(
    "Vendor",
    {
      Vendor_ID: {
        primaryKey: true,
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        validate: {
          notEmpty: {
            msg: "Ticket cannot be empty",
          },
        },
        allowNull: false,
      },
      VendorName: DataTypes.STRING,
      PhoneNumber: DataTypes.STRING,
      address: DataTypes.STRING,
      createdAt: DataTypes.STRING,
      updatedAt: DataTypes.STRING,
      deletedAt: DataTypes.STRING,
    },
    {
      timestamps: false,
    }
  );
  return Vendor;
};
