module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define("User", {
    user_id: {
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      validate: {
        notEmpty: {
          msg: "Ticket cannot be empty",
        },
      },
      allowNull: false,
    },
    name: DataTypes.STRING,
    phoneNumber: DataTypes.STRING,
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: {
          msg: "Ticket cannot be empty",
        },
        isEmail: {
          args: true,
          msg: "email invalid",
        },
      },
      unique: {
        args: true,
        msg: "Email address already in use!",
      },
    },
    password: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          msg: "Password cannot be empty",
        },
      },
    },
    createdAt: DataTypes.STRING,
    updatedAt: DataTypes.STRING,
  });
  return User;
};
