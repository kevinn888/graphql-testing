const { gql } = require("apollo-server");

const userTypeDef = gql`
  type Product {
    Product_ID: String!
    ProductName: String
    Quantity: Int
    Vendor_ID: String
    created_at: String!
    updated_at: String
    deleted_at: String
  }
  extend type Query {
    products: [Product]!
    product(Product_ID: ID!): Product
  }
  extend type Mutation {
    newProduct(ProductName: String, Quantity: Int): Product!
    updateProduct(Product_ID: ID!, Quantity: Int!): Product!
    deleteProduct(Product_ID: ID!): Product!
  }
`;

module.exports = userTypeDef;
