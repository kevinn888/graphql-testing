const authResolvers = require("./AuthResolver");
const productResolvers = require("./ProductResolver");
const vendorResolvers = require("./VendorResolver");

const resolvers = [authResolvers, productResolvers, vendorResolvers];

module.exports = resolvers;
