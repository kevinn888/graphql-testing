const { gql } = require("apollo-server");

const userTypeDef = gql`
  type User {
    user_id: String!
    name: String
    email: String!
    PhoneNumber: String
    password: String!
    createdAt: String!
    updatedAt: String!
  }
`;

module.exports = userTypeDef;
