const Redis = require("ioredis");
const client = new Redis(
  "redis://h:pef0bdee8264ed107f68808f4fa3140249cd9bb0b4e5b02f1e46957ac021df736@ec2-23-23-149-97.compute-1.amazonaws.com:31669"
);
client.on("error", function (err) {
  console.log("Error Redis:" + err);
});
client.on("connect", function () {
  console.log("Connection establish");
});

module.exports = client;
