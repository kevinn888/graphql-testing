const dotenv = require("dotenv");
dotenv.config({ debug: process.env.DEBUG });
module.exports = {
  DBUsername: process.env.DBUSERNAME,
  DBPassword: process.env.PASSWORD,
  Database: process.env.DATABASE,
  DBHost: process.env.HOST,
  DBPort: process.env.DBPORT,
  Dialect: process.env.DIALECT,
};
