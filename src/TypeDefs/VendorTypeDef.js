const { gql } = require("apollo-server");

const vendorTypeDef = gql`
  type Vendor {
    Vendor_ID: String!
    VendorName: String
    PhoneNumber: String
    address: String
    createdAt: String!
    updatedAt: String!
    deletedAt: String
  }
  extend type Query {
    vendors: [Vendor]!
    vendor(Vendor_ID: ID!): Vendor
  }
  extend type Mutation {
    newVendor(
      VendorName: String!
      PhoneNumber: String
      address: String
    ): Vendor!
  }
`;

module.exports = vendorTypeDef;
