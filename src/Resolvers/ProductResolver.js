const authenticated = require("../Utils/Authorization");

const resolvers = {
  Query: {
    products: authenticated((parent, args, { db }) => db.Product.findAll()),
    product: authenticated((parent, args, { db }) =>
      db.Product.findByPk(args.Product_ID)
    ),
  },

  Mutation: {
    newProduct: authenticated(async (parent, args, { db }) => {
      const timestamp = Math.round(new Date().getTime() / 1000);
      const lastId = (await redis.get("productId")) || 1 + 1;
      const product = {
        Product_ID: "PRD-" + String(lastId),
        ProductName: args.ProductName || "",
        Quantity: args.Quantity || "",
        Vendor_ID: args.Vendor_ID,
        created_at: timestamp.toString(),
        updated_at: timestamp.toString(),
        deleted_at: "",
      };
      const check = await db.Product.create(product);
      return check;
    }),
    updateProduct: authenticated(async (parent, args, { db }) => {
      // const oldProduct = db.Product.findByPk(args.Product_ID);
      const newProduct = {
        Product_ID: args.Product_ID,
        Quantity: args.Quantity,
      };
      const check = db.Product.update(newProduct, {
        where: { Product_ID: args.Product_ID },
      });
      return check;
    }),
    deleteProduct: authenticated(async (parent, args, { db }) => {
      const result = await db.Product.destroy({
        where: {
          Product_ID: args.Product_ID,
        },
      });
      return result;
    }),
  },
};

module.exports = resolvers;
