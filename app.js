const { ApolloServer } = require("apollo-server");
const typeDefs = require("./src/TypeDefs");
const resolvers = require("./src/Resolvers");
const db = require("./src/Models");
const redis = require("./src/Config/Redis");
const bcrypt = require("bcrypt");

const server = new ApolloServer({
  typeDefs,
  resolvers,
  context: ({ req }) => {
    const accessToken = req.headers.authorization || "";
    const authorization = accessToken.replace("Bearer ", "");
    return { accessToken: authorization, db, redis };
  },
  introspection: true,
  playground: true,
});

server
  .listen({ port: process.env.PORT || 4000 })
  .then(({ url }) => {
    console.log(`🚀 server ready at ${url}`);
  })
  .catch((error) => {
    consola.error(error);
  });
