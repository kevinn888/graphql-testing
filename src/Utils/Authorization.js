const { getPayload } = require("./JWT");
const authenticated = (next) => (root, args, context, info) => {
  if (!context.accessToken) {
    throw new Error(`Unauthorized Access`);
  }
  const payload = getPayload(context.accessToken);
  if (payload.loggedIn) return next(root, args, context, info);
  throw new Error(payload.msg);
};

module.exports = authenticated;
