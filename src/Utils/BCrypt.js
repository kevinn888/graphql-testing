const bcrypt = require("bcrypt");

const encrypt = (password) => {
  const saltRound = 15;
  const hashedPassword = bcrypt.hashSync(password, saltRound);
  return hashedPassword;
};

const comparePassword = (password, hashedPassword) => {
  const compare = bcrypt.compareSync(password, hashedPassword);
  return compare;
};

module.exports = { encrypt, comparePassword };
