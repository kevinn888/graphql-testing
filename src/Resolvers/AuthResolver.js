const {
  getPayload,
  createToken,
  encrypt,
  comparePassword,
} = require("../Utils");
const jwt = require("jsonwebtoken");

const { AuthenticationError } = require("apollo-server");

const resolver = {
  Query: {
    whoami: async (parent, args, { accessToken, db, redis }) => {
      if (!accessToken) throw new Error("unauthorized access");
      const tokenExist = await redis.exists(accessToken);
      const user = getPayload(accessToken);
      if (user.loggedIn == true && Boolean(tokenExist)) {
        const dataUser = await db.User.findByPk(user.payload.userId);
        return dataUser;
      }
      throw new jwt.JsonWebTokenError(user.msg);
    },
  },

  Mutation: {
    authenticate: async (parent, args, { db, redis }) => {
      const { email, password } = args;
      const data = await db.User.findOne({ where: { email } });
      console.log(data.user_id);
      if (data && comparePassword(password, data.password)) {
        const token = createToken(data.user_id);
        redis.set(token, data.user_id);
        redis.expire(token, 14 * 24 * 60 * 60);
        return { accessToken: `Bearer ${token}` };
      }
      throw new AuthenticationError("Invalid password or email");
    },
  },
};

module.exports = resolver;
