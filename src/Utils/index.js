const { getPayload, createToken } = require("./JWT");
const { encrypt, comparePassword } = require("./BCrypt");
module.exports = {
  getPayload,
  createToken,
  encrypt,
  comparePassword,
};
