const jwt = require("jsonwebtoken");
const fs = require("fs");

const getPayload = (token) => {
  const public_key = fs.readFileSync("./id_rsa.pub", "utf8");
  const payload = jwt.verify(token, public_key);
  if (payload) {
    const timestamp = Math.round(new Date().getTime() / 1000);
    if (payload.exp > timestamp) {
      return { loggedIn: true, payload };
    } else {
      return { loggedIn: false, msg: "token has expired" };
    }
  }
  // Failed Login Status
  return { loggedIn: false, msg: "token is invalid" };
};

const createToken = (userId) => {
  const private_key =
    process.env.PRIVATE_KEY || fs.readFileSync("./id_rsa", "utf8");
  const token = jwt.sign({ userId }, private_key, {
    expiresIn: "336h",
    algorithm: "RS256",
  });
  return token;
};
module.exports = { getPayload, createToken };
